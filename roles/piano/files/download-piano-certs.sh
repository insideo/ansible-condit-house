#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" >&2
  exit 1
fi

domain=condit.house
set -e

for file in cert.pem chain.pem fullchain.pem privkey.pem ; do
  scp \
	-q -p -o StrictHostKeyChecking=accept-new -o BatchMode=yes \
	-i /root/.ssh/id_certs \
	"condithouse@sy.ndro.me:/srv/certs/${domain}/${file}" \
	"/etc/nghttpx/ssl/${domain}-incoming/"
done

diff -r \
	"/etc/nghttpx/ssl/${domain}" \
	"/etc/nghttpx/ssl/${domain}-incoming" >/dev/null 2>&1 || (

  for f in /etc/nghttpx/ssl/${domain}-incoming/* ; do
    cp -f "${f}" "/etc/nghttpx/ssl/${domain}/"
  done
  systemctl -q restart nghttpx.service

)
